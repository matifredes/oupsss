# Generated by Django 3.2.3 on 2021-07-14 03:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('usuario', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usuario',
            name='direccion',
            field=models.CharField(default='', max_length=300, null=True, verbose_name='Dirección'),
        ),
    ]
